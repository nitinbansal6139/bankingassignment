package com.greatlearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomerUtils {

    private static final List<CustomerLoginInfo> infoList = new ArrayList<>();

    static {
        infoList.add(new CustomerLoginInfo("1001", "pwd1"));
        infoList.add(new CustomerLoginInfo("1002", "pwd2"));
        infoList.add(new CustomerLoginInfo("1003", "pwd3"));
        infoList.add(new CustomerLoginInfo("1004", "pwd4"));
        infoList.add(new CustomerLoginInfo("1005", "pwd5"));
    }

    public static boolean isAuthenticated(String accountNumber, String password) {
        Optional<CustomerLoginInfo> authenticatedCustomer = infoList.stream().
                filter(c -> c.getBankAccountNo().equals(accountNumber) && c.getPassword().equals(password))
                .findFirst();
        return authenticatedCustomer.isPresent();
    }

}
