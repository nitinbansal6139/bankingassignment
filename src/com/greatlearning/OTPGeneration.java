package com.greatlearning;

import java.util.Random;

public class OTPGeneration {
    public static int generateOtpAndSendSMS() {
        Random random = new Random();
        int randomOtp = random.nextInt(10000);
        SmsGeneration.sendSms(String.valueOf(randomOtp), "8105886894");
        return randomOtp;
    }
}
