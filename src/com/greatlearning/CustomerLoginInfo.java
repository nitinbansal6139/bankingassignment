package com.greatlearning;

public class CustomerLoginInfo {

    private String bankAccountNo;
    private String password;

    public CustomerLoginInfo(String bankAccountNo, String password) {
        this.bankAccountNo = bankAccountNo;
        this.password = password;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
