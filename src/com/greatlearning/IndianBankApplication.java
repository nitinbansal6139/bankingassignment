package com.greatlearning;

import java.io.*;
import java.util.Scanner;

public class IndianBankApplication {

    public static void main(String[] args) throws InterruptedException, IOException {
        System.out.println("Welcome to the login page");
        Thread.sleep(1000);
        System.out.println("Enter the bank account no.");
        try (Scanner scanner = new Scanner(System.in)) {
            String bankAccountNo = scanner.next();
            System.out.println("Enter the password.");
            String password = scanner.next();
            if (CustomerUtils.isAuthenticated(bankAccountNo, password)) {
                displayBankingOperations(scanner);
            } else {
                System.out.println("Invalid Credentials");
            }
        }
    }

    private static void displayBankingOperations(Scanner scanner) throws IOException {
        boolean continueExecution = true;
        System.out.println("!!!! Welcome to Indian Bank !!!!!");
        System.out.println("----------------------------------");
        try(FileWriter writer = new FileWriter("banking-transactions.log", true);) {
            do {
                System.out.println("Enter the operation that you want to perform");
                System.out.println("1. Deposit");
                System.out.println("2. Withdrawal");
                System.out.println("3. Transfer");
                System.out.println("0. Logout");
                int operation = scanner.nextInt();
                switch (operation) {
                    case 1:
                        System.out.println("Enter the amount you want to deposit");
                        int amount = scanner.nextInt();
                        System.out.println("amount " + amount + " deposited successfully");
                        writer.write("amount "+amount+ " deposited successfully\n");
                        break;
                    case 2:
                        System.out.println("Enter the amount you want to withdraw");
                        int withdrawalAmount = scanner.nextInt();
                        System.out.println("amount " + withdrawalAmount + " withdrawal successfully");
                        writer.write("amount " + withdrawalAmount + " withdrawal successfully\n");
                        break;
                    case 3:
                        System.out.println("Enter the OTP");
                        otpGenerationVerificationLogic(scanner, writer);
                        break;
                    case 0:
                        continueExecution = false;
                        break;
                    default:
                        System.out.println("Input 0-3 number");
                        break;
                }
            } while (continueExecution);
        }
    }

    private static void otpGenerationVerificationLogic(Scanner scanner, FileWriter writer) throws IOException {
        int generatedOtp = OTPGeneration.generateOtpAndSendSMS();
        int enteredOTP = scanner.nextInt();
        if (generatedOtp == enteredOTP) {
            System.out.println("OTP verification successful !!!!");
            System.out.println("Enter the amount and bank account no to which you want to transfer");
            int amountToTransfer = scanner.nextInt();
            String bankAccountForTransfer = scanner.next();
            System.out.println("Amount " + amountToTransfer + " transferred successfully to bank account " + bankAccountForTransfer);
            writer.write("Amount " + amountToTransfer + " transferred successfully to bank account " + bankAccountForTransfer+"\n");

        } else {
            System.out.println("OTP is invalid");
        }
    }
}
